﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrazyComplet.Web.Security
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string Member = "Member";
    }
}

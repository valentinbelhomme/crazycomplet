﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrazyComplet.Lib.Models;
using CrazyComplet.Lib.Services;
using CrazyComplet.Lib.Services.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CrazyComplet.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        ILoginService _loginService { get; set; }

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }
        //IUserService UsersService { get; set; }

        //public LoginController(IUserService usersService)
        //{
        //    UsersService = usersService;
        //}

        // POST: api/Login
        [HttpPost]
        public async Task<User> Post([FromBody] LoginRequest loginRequest)
        {
            return await Task.Run(() =>
            {
                return _loginService.Authenticate(loginRequest);
            });
        }
        //public IActionResult Post([FromBody] LoginRequest request)
        //{
        //    var user = UsersService.Authenticate(request.Email, request.Password);

        //    if (user == null)
        //        return BadRequest(new { message = "Username or password is incorrect" });

        //    return Ok(user);
        //}
    }
}

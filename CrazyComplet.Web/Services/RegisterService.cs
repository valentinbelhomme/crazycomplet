﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrazyComplet.Lib.DAL;
using CrazyComplet.Lib.Models;
using CrazyComplet.Lib.Services.Dtos;

namespace CrazyComplet.Lib.Services
{
    public class RegisterService : IRegisterService
    {
        public ILoginService LoginService { get; set; }

        public CrazyCompletDbContext DbContext { get; set; }

        public RegisterService(CrazyCompletDbContext dbContext, ILoginService loginService)
        {
            DbContext = dbContext;
            LoginService = loginService;
        }


        public virtual RegisterResponse Register(RegisterRequest registerRequest)
        {
            var output = new RegisterResponse();

            if (string.IsNullOrEmpty(registerRequest.Email))
            {
                output.Status = RegisterResponseStatus.MissingEmail;
            }
            else if (!registerRequest.Email.Contains("@"))
            {
                output.Status = RegisterResponseStatus.WrongEmail;
            }
            else if (string.IsNullOrEmpty(registerRequest.Password))
            {
                output.Status = RegisterResponseStatus.MissingPassword;
            }
            else if (registerRequest.Password.Length < 8)
            {
                output.Status = RegisterResponseStatus.PasswordInsecure;
            }

            var member = DbContext.Members.FirstOrDefault(x => x.Email == registerRequest.Email);

            if (member != null)
            {
                output.Status = RegisterResponseStatus.UserWithEmailAlreadyExists;
            }
            else
            {
                member = new Member
                {
                    Email = registerRequest.Email,
                    Password = registerRequest.Password,
                    Name = registerRequest.Name,
                    Surname = registerRequest.Surname
                     
                };

                DbContext.Members.Add(member);
                DbContext.SaveChanges();

                output.Status = RegisterResponseStatus.Ok;

                var loginRequest = new LoginRequest()
                {
                    Email = registerRequest.Email,
                    Password = registerRequest.Password
                };

                output.Member = LoginService.Authenticate(loginRequest) as Member;
            }

            return output;

        }
    }
}

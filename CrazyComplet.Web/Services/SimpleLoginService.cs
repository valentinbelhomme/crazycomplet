﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using CrazyComplet.Lib.Models;
using CrazyComplet.Lib.Services;
using CrazyComplet.Lib.Services.Dtos;
using CrazyComplet.Web.Helpers;
using CrazyComplet.Lib.DAL;
using System.Linq;

namespace CrazyComplet.Lib.Services
{
    public class SimpleLoginService : ILoginService
    {
        CrazyCompletDbContext DbContext { get; set; }

        public SimpleLoginService(CrazyCompletDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual User Authenticate(LoginRequest loginRequest)
        {
            if (DbContext.Admins.Count() == 0)
            {
                DbContext.Admins.Add(new Admin()
                {
                    Id = Guid.NewGuid(),
                    Name = "sys",
                    Surname = "Belhomme",
                    Email = "a@a",
                    Password = "1234"
                    //IsMaster = true
                });

                DbContext.SaveChanges();
            }

            var user = DbContext.Users.FirstOrDefault(x => x.Email == loginRequest.Email && x.Password == loginRequest.Password);

            return user;
        }
    }
}

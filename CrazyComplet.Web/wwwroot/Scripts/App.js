﻿var app = angular.module('CrazyComplet', ['ngAnimate', 'ngRoute', 'ui.grid', 'ui.bootstrap']);

app.config(function ($routeProvider, $locationProvider)
{
    $routeProvider.when('/admins',
        {
            template: '<admins></admins>'
        });

    $routeProvider.when('/books',
        {
            template: '<books></books>'
        });

    $routeProvider.when('/lends',
        {
            template: '<lends></lends>'
        }); 

    $routeProvider.when('/members',
        {
            template: '<members></members>'
        });

    $routeProvider.when('/requests',
        {
            template: '<requests></requests>'
        });

    $routeProvider.when('/rooms',
        {
            template: '<rooms></rooms>'
        });

    $routeProvider.when('/users',
        {
            template: '<users></users>'
        });
    $routeProvider.when('/login',
        {
            template: '<login></login>'
        });

    $routeProvider.when('/register',
        {
            template: '<register></register>'
        });

    $routeProvider.when('/start',
        {
            template: '<start></start>'
        });

    $routeProvider.when('/home',
        {
            template: '<home></home>'
        });
    $routeProvider.when('/loading',
        {
            template: '<loading></loading>'
        });

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
});
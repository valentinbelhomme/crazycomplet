﻿class Room extends Entity
{
    constructor(json)
    {
        if (json)
        {
            super(json);
            this.Code = json.code;
            this.Color = json.color;
            this.Capacity = json.capacity;
            this.Accessibility = json.accessibility;

        }
        else
        {
            super();
            this.Code = "";
            this.Color = "#000000";
            this.Capacity = 0;
            this.Accessibility = false;
        }

       
    }
}
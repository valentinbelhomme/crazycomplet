﻿class Book extends Entity
{
    get ComposedName()
    {
        return this.Title + ", " + this.Author;
    }

    constructor(json)
    {
        if (json)
        {
            super(json);
            this.Title = json.title;
            this.Author = json.author;
            this.Year = json.year;
            this.Amount = json.amount;
        }

        else
        {
            super();
            this.Title = "";
            this.Author = "";
            this.Year = 0;
            this.Amount = 0;
        }
    }
//    class Room extends Entity
//{
//    constructor(json)
//    {
//        if (json)
//        {
//            super(json);
//            this.Number = json.number;
//            this.AreaId = json.areaId;
//            this.FloorId = json.floorId;
//            this.FloorNumber = json.floorNumber;
//        }

//        else
//        {
//            super();
//            this.Number = 0;
//            this.AreaId = "00000000-0000-0000-0000-000000000000";
//            this.FloorId = "00000000-0000-0000-0000-000000000000";
//            this.FloorNumber = 0;
//        }
//    }
//}
}
﻿class Request extends Entity
{
    constructor(room, hourIn, hourOff, user, json)
    {
        this.HourIn = hourIn;
        this.HourOff = hourOff;
        this.Room = room;
        this.User = user;

        if (json)
        {
            super(json)
            this.UserId = json.userId;
            //this.RequestUserName = user === undefined ? "" : user.Name;
            this.HourIn = json.hourIn;
            this.HourOff = json.hourOff;
            this.RoomId = json.roomId;
            //this.RequestRoomCode = room === undefined ? "" : room.Code;
        }

        else
        {
            super();
            this.UserId = "00000000-0000-0000-0000-000000000000";
            //this.RequestUserName = user === undefined ? "" : user.Name;
            this.HourIn = hourIn;
            this.HourOff = hourOff;
            this.RoomId = "00000000-0000-0000-0000-000000000000";
            //this.RequestRoomCode = room === undefined ? "" : room.Code;
        }
        



        
        
    }
}
﻿class Lend extends Entity
{
    constructor(json)
    {
        super(json);

        if (json)
        {
            this.Book = json.book;
            this.Member = json.member;
            this.LendedOn = json.lendedOn;
            this.ExpiresOn = json.expiresOn;
            this.BookComposedName = json.bookComposedName;
            //this.LendBookTitle = book === undefined ? "" : book.Title;
        }

        else
        {
            this.Book = null;
            this.Member = null;
            this.LendedOn = new Date();
            this.LendedOn = (this.LendedOn.getDate() + "/" +
                ("0" + (this.LendedOn.getMonth() + 1)).slice(-2) + "/" +
                this.LendedOn.getFullYear());

            this.ExpiresOn = new Date();
            this.ExpiresOn = (this.ExpiresOn.getDate() + "/" +
                ("0" + (this.ExpiresOn.getMonth() + 2)).slice(-2) + "/" +
                this.ExpiresOn.getFullYear());
        }
    }
}
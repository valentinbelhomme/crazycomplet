﻿class Member extends User
{
    get ComposedName()
    {
        return this.Name + " " + this.Surname;
    }

    constructor(json)
    {
        if (json)
        {
            super(json);
        }

        else
        {
            super();
        }
    }
}
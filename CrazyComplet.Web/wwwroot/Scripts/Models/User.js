﻿class User extends Entity
{
    constructor(json)
    {
        if (json)
        {
            super(json);
            this.Name = json.name;
            this.Surname = json.surname;
            this.Email = json.email;
            this.Password = json.password;
            this.UserType = json.userType;
        
        }

        else
        {
            super();
            this.Name = "";
            this.Surname = "";
            this.Email = "";
            this.Password = "";
            this.UserType = "";
        }
    }
}
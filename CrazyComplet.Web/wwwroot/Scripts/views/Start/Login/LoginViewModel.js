﻿class LoginViewModel
{
    constructor($LoginService, $window)
    {
        this.LoginSvc = $LoginService;
        this.Window = $window;

        this.Window.AccountUser = true;
    }   

    Login()
    {
        this.Window.IsLoading = true;
        this.LoginSvc.LoginAsync(this.Email, this.Password)
            .then((response) =>
            {
                if (response.data !== "")
                {
                    this.Window.Token = response.data.token;
                    this.Window.LogonUser = response.data;
                    //console.log(this.Window.LogonUser);
                    //console.log(response.data.token);
                }
                else
                {
                    alert("Usuario incorrecto");
                    console.log(response.data);
                }
                this.Window.IsLoading = false;
            },
                (response) =>
                {
                    alert(response.data.message);
                    console.log(response.data);
                    this.Window.Token = null;

                    this.Window.IsLoading = false;
                });
    }

    ForgotPassword()
    {
        alert("So bad..");
    }

    RedirectRegister()
    {
        //this.Location.path("/register");
        this.Window.AccountUser = false;
    }

    ClearForm()
    {
        this.Email = "";
        this.Password = "";

        Email.classList.add("ng-pristine");
        Password.classList.add("ng-pristine");
    }
}

app.component('login',
    {
        templateUrl: './Scripts/views/Start/Login/LoginView.html',
        controller: LoginViewModel,
        controllerAs: "vm"
    });
﻿class StartViewModel
{
    constructor($http, $window)
    {
        this.Http = $http;
        this.Window = $window;
    }

    get HaveAccount()
    {
        if (this.Window.AccountUser)
            return true;
        else
        {
            return false;
        }
    }

}

app.component('start',
    {
        templateUrl: './Scripts/views/Start/StartView.html',
        controller: StartViewModel,
        controllerAs: "vm"
    });
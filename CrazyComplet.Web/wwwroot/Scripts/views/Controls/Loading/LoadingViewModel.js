﻿class LoadingViewModel
{
    constructor()
    {

    }
}
app.component('loading',
    {
        templateUrl: './Scripts/views/Controls/Loading/LoadingView.html',
        controller: LoadingViewModel,
        controllerAs: "vm"
    });
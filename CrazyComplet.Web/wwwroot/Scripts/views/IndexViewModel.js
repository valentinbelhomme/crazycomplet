﻿class IndexViewModel
{
    constructor($window)
    {
        //this.Http = $http;
        //this.Location = $location;
        this.Window = $window;
    }

    ShowView(option)
    {
        this.Location.path("/" + option);
    }

    IsLogon()
    {
        if (!this.Window || this.Window.LogonUser === null || this.Window.LogonUser === undefined)
            return false;
        else
            return true;
    }
    //IsLogon()
    //{
    //    if (this.Window.LogonUser)
    //        return true;
    //    else
    //    {
    //        return false;
    //    }
    //}

    IsStartVisible()
    {
        let c1 = !this.IsLogon();
        let c2 = !this.Window.IsLoading;
        return c1 && c2;
    }

    IsLoading()
    {
        return this.Window.IsLoading;
    }
}

app.component('index',
{
    templateUrl: './Scripts/views/IndexView.html',
    controller: IndexViewModel,
    controllerAs: "vm"
});

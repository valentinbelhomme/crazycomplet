﻿class AdminsViewModel
{
    constructor($AdminsService)
    {
        this.AdminSvc = $AdminsService;

        this.GetAllAdmins();
        this.Admins = [];

        this.SelectedAdmin = null;
        this.IsEditing = false;
        

        this.GridOptions =
            {
                enableFiltering: false,
                enableSorting: false,
                data: 'vm.Admins',
                appScopeProvider: this,
                columnDefs:
                    [
                        { name: 'Full Name', field: 'Full Name', cellTemplate: '<span>{{row.entity.Name}} {{row.entity.Surname}}</span>', cellClass: "ui-grid-cell-contents" },
                        { name: 'Email', field: 'Email' },
                        { name: '', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Select" ng-click="grid.appScope.SelectAdmins(row.entity)"></div>' },
                        { name: ' ', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Delete" ng-click="grid.appScope.DeleteAdmins(row.entity)"></div>' }
                    ]
            };

        this.EmailClass = "";
    }

    GetAllAdmins()
    {
        this.AdminSvc.GetAllAsync()
            .then((response) =>
            {
                this.OnGetData(response);
            });
    }

    OnGetData(response)
    {
        this.Admins.length = 0;
        for (let i in response.data)
        {
           let admin = new Admin(response.data[i]);
            this.Admins.push(admin);
        }
    }

    AddNewAdmin()
    {
        let admin = new Admin();

        admin.Name = this.Name;
        admin.Surname = this.Surname;
        admin.Email = this.Email;
        admin.Password = this.Password;
        admin.IsMaster = this.IsMaster;

        this.AddAdmin(admin);
    }

    AddAdmin(admin)
    {
        this.AdminSvc.AddAsync(admin)
        //var config = { headers: { 'Content-Type': 'application/json' } };
        //this.Http.post("api/admin", admin, config)
            .then((response) =>
            { this.OnAdminAdded(response); },
                response => console.log("error"));
    }

    OnAdminAdded(response)
    {
        let admin = new Admin(response.data);
        this.Admins.push(admin);
        this.ClearForm();
    }

    SelectAdmins(admin)
    {
        this.SelectedAdmin = admin;

        this.Name = admin.Name;
        this.Surname = admin.Surname;
        this.Email = admin.Email;
        this.Password = admin.Password;
        this.IsMaster = admin.IsMaster;

        this.IsEditing = true;
    }

    UpdateAdmin()
    {
        this.SelectedAdmin.Name = this.Name;
        this.SelectedAdmin.Surname = this.Surname;
        this.SelectedAdmin.Email = this.Email;
        this.SelectedAdmin.Password = this.Password;
        //this.SelectedAdmin.IsMaster = this.IsMaster;

        this.AdminSvc.UpdateAsync(this.SelectedAdmin)
            .then((response) =>
            {
                //response.data = this.SelectedAdmin;
                this.OnAdminUpdated(response);
            },
                response => console.log(response));
    }

    OnAdminUpdated(response)
    {
        //let admin = new Admin(response.data);
        //let index = this.Admins.findIndex(x => x.Id == this.SelectedAdmin.Id);
        //this.Admins[index] = admin;

        console.log(response.data);

        this.ClearForm();
    }

    DeleteAdmins(admin)
    {
        //let url = "api/admins/" + admin.Id;
        //this.Http.delete(url)
        this.AdminSvc.DeleteAsync(admin)
            .then((response) =>
            {
                this.OnAdminDeleted(admin);
                console.log(admin);
            },
                (response) => console.log(response));
    }

    OnAdminDeleted(admin)
    {
        let i = this.Admins.findIndex((x) => x.Id == admin.Id);
        this.Admins.splice(i, 1);
    }

    ClearForm()
    {
        this.Name = "";
        this.Surname = "";
        this.Email = "";
        this.Password = "";
        this.IsMaster = false;

        this.IsEditing = false;

        Name.classList.add("ng-pristine");
        Surname.classList.add("ng-pristine");
        this.EmailClass = "ng-pristine";
        this.PasswordClass = "ng-pristine";
    }
}

app.component('admins',
    {
        templateUrl: './Scripts/views/Home/Admins/AdminsView.html',
        controller: AdminsViewModel,
        controllerAs: "vm"
    });



﻿class BooksViewModel
{
    constructor($BooksService)
    {
        this.BooksSvc = $BooksService;
        this.GetAllBooks();
        this.Books = [];

        this.SelectedBook = null;
        this.IsEditing = false;

        this.GridOptions =
            {
                enableFiltering: false,
                enableSorting: false,
                data: 'vm.Books',
                appScopeProvider: this,
                columnDefs:
                    [
                        { name: 'Title', field: 'Title' },
                        { name: 'Author', field: 'Author' },
                        { name: 'Year', field: 'Year' },
                        { name: 'Amount', field: 'Amount' },
                        { name: '', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Select" ng-click="grid.appScope.SelectBooks(row.entity)"></div>' },
                        { name: ' ', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Delete" ng-click="grid.appScope.DeleteBooks(row.entity)"></div>' }
                    ]
            }
    }

    GetAllBooks()
    {
        this.BooksSvc.GetAllAsync()
            .then((response) =>
            {
                this.OnGetData(response);
            });
    }

    OnGetData(response)
    {
        this.Books.length = 0;
        for (let i in response.data)
        {
            let book = new Book(response.data[i]);
            this.Books.push(book);
        }
    }

    AddNewBook()
    {
        let book = new Book();

        book.Title = this.Title;
        book.Author = this.Author;
        book.Year = this.Year;
        book.Amount = this.Amount;
        
        this.BooksSvc.AddAsync(book)
        //var config = { headers: { 'Content-Type': 'application/json' } };
        //this.Http.post("api/clients", client, config)
            .then((response) =>
            { this.OnBookAdded(response); },
                response => console.log("error"));
    }

    OnBookAdded(response)
    {
        let book = new Book(response.data);
        this.Books.push(book);
        this.ClearForm();
    }

    SelectBooks(book)
    {
        this.SelectedBook = book;

        this.Title = book.Title;
        this.Author = book.Author;
        this.Year = book.Year;
        this.Amount = book.Amount;

        this.IsEditing = true;
    }

    UpdateBook()
    {
        this.SelectedBook.Title = this.Title;
        this.SelectedBook.Author = this.Author;
        this.SelectedBook.Year = this.Year;
        this.SelectedBook.Amount = this.Amount;

        this.BooksSvc.UpdateAsync(this.SelectedBook)
            .then((response) =>
            {
                //response.data = this.SelectedBook;
                this.OnBookUpdated(response);
            },
                response => console.log(response));
    }

    OnBookUpdated(response)
    {
        //let book = new Book(response.data);
        //let index = this.Books.findIndex(x => x.Id == this.SelectedBook.Id);
        //this.Books[index] = book;

        //console.log(response.data);

        this.ClearForm();
    }

    DeleteBooks(book)
    {
        //let url = "api/clients/" + client.Id;
        //this.Http.delete(url)
        this.BooksSvc.DeleteAsync(book)
            .then((response) =>
            {
                this.OnBookDeleted(book);
                console.log(book);
            },
                (response) => console.log(response));
    }

    OnBookDeleted(book)
    {
        let i = this.Books.findIndex((x) => x.Id === book.Id);
        this.Books.splice(i, 1);
    }

    ClearForm()
    {
        this.Title = "";
        this.Author = "";
        this.Year = "";
        this.Amount = "";

        this.IsEditing = false;

        Title.classList.add("ng-pristine");
        Author.classList.add("ng-pristine");
        Year.classList.add("ng-pristine");
        Amount.classList.add("ng-pristine");
    }
}

app.component('books',
    {
        templateUrl: './Scripts/views/Home/Books/BooksView.html',
        controller: BooksViewModel,
        controllerAs: "vm"
    });



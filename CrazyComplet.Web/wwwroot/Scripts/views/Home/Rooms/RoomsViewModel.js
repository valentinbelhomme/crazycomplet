﻿class RoomsViewModel
{
    constructor($RoomsService)
    {
        this.RoomsSvc = $RoomsService;
        this.GetAllRooms();
        this.Rooms = [];

        this.SelectedRoom = null;
        this.IsEditing = false;

        this.GridOptions =
            {
                enableFiltering: false,
                enableSorting: false,
                data: 'vm.Rooms',
                appScopeProvider: this,
                columnDefs:
                    [
                        { name: 'Code', field: 'Code' },
                        { name: 'Color', field: 'Color', cellTemplate: "<div class=\"ui-grid-cell-contents ng-scope ng-binding\" ng-style=\"{'background-color':COL_FIELD}\">" },
                        { name: 'Capacity', field: 'Capacity' },
                        { name: 'Accessibility', field: 'Accessibility' },
                        { name: '', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Select" ng-click="grid.appScope.SelectRooms(row.entity)"></div>' },
                        { name: ' ', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Delete" ng-click="grid.appScope.DeleteRooms(row.entity)"></div>' }
                    ]
            }
    }

    GetAllRooms()
    {
        this.RoomsSvc.GetAllAsync()
            .then((response) =>
            {
                this.OnGetData(response);
            });
    }

    OnGetData(response)
    {
        this.Rooms.length = 0; 
        for (let i in response.data)
        {
            let room = new Room(response.data[i]);
            this.Rooms.push(room);
        }
    }

    AddNewRoom()
    {
        let room = new Room();

        room.Code = this.Code;
        room.Color = this.Color;
        room.Capacity = this.Capacity;
        room.Accessibility = this.Accessibility;
        
        this.RoomsSvc.AddAsync(room)
        //var config = { headers: { 'Content-Type': 'application/json' } };
        //this.Http.post("api/clients", client, config)
            .then((response) =>
            { this.OnRoomAdded(response); },
                response => console.log("error"));
    }

    OnRoomAdded(response)
    {
        let room = new Room(response.data);
        this.Rooms.push(room);
        this.ClearForm();
    }

    SelectRooms(room)
    {
        this.SelectedRoom = room;

        this.Code = room.Code;
        this.Color = room.Color;
        this.Capacity = room.Capacity;
        this.Accessibility = room.Accessibility;

        this.IsEditing = true;
    }

    UpdateRoom()
    {
        this.SelectedRoom.Code = this.Code;
        this.SelectedRoom.Color = this.Color;
        this.SelectedRoom.Capacity = this.Capacity;
        this.SelectedRoom.Accessibility = this.Accessibility;

        this.RoomsSvc.UpdateAsync(this.SelectedRoom)
            .then((response) =>
            {
                //response.data = this.SelectedRoom;
                this.OnRoomUpdated(response);
            },
                response => console.log(response));
    }

    OnRoomUpdated(response)
    {
        //let room = new Room(response.data);
        //let index = this.Rooms.findIndex(x => x.Id == this.SelectedRoom.Id);
        //this.Rooms[index] = room;

        //console.log(response.data);

        this.ClearForm();
    }

    DeleteRooms(room)
    {
        //let url = "api/clients/" + client.Id;
        //this.Http.delete(url)
        this.RoomsSvc.DeleteAsync(room)
            .then((response) =>
            {
                this.OnRoomDeleted(room);
                console.log(room);
            },
                (response) => console.log(response));
    }

    OnRoomDeleted(room)
    {
        let i = this.Rooms.findIndex((x) => x.Id === room.Id);
        this.Rooms.splice(i, 1);
    }

    ClearForm()
    {
        this.Code = "";
        this.Color = "";
        this.Capacity = "";
        this.Accessibility = false;

        this.IsEditing = false;

        Code.classList.add("ng-pristine");
        Color.classList.add("ng-pristine");
        Capacity.classList.add("ng-pristine");
        
    }
}

app.component('rooms',
    {
        templateUrl: './Scripts/views/Home/Rooms/RoomsView.html',
        controller: RoomsViewModel,
        controllerAs: "vm"
    });



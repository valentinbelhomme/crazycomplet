﻿class HeaderViewModel
{
    constructor($location, $window)
    {
        this.Location = $location;
        this.Window = $window;
    }

    ShowView(option)
    {
        this.Location.path("/" + option);
    }

    ShowUserName()
    {
        if (this.Window && this.Window.LogonUser && this.Window.LogonUser.member)
            return this.Window.LogonUser.member.name;

        else if (this.Window && this.Window.LogonUser)
            return this.Window.LogonUser.name;
        
        else
            return "";
    }
}

app.component('header',
    {
        templateUrl: './Scripts/views/Home/Header/HeaderView.html',
        controller: HeaderViewModel,
        controllerAs: "vm", 
        function($scope)
        {
            $scope.ShowView('users')
        }
    });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
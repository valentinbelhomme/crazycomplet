﻿class LendsViewModel
{
    get SelectedMember() 
    {
        return this._selectedMember;
    }
    set SelectedMember(value) 
    {
        this._selectedMember = value;
        this.GetMemberLends();
    }

    constructor($MembersService, $BooksService, $LendsService)
    {
        this.LendsSvc = $LendsService;
        this.BooksSvc = $BooksService;
        this.MembersSvc = $MembersService;

        this.Books = [];
        this.Members = [];
        this.Lends = [];

        this._selectedBook = null;
        this._selectedMember = null;

        //this.IsEditing = false;

        this.GetAllBooks();
        this.GetAllMembers();

        this.GridOptions =
            {
                enableFiltering: false,
                enableSorting: false,
                data: 'vm.Lends',
                appScopeProvider: this,
                columnDefs:
                    [
                        { name: 'Book', field: 'BookComposedName' },
                        { name: '', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP"><input type="button" style="width: 80px;" value="Select" ng-click="grid.appScope.SelectBook(row.entity)"><input type="button" style="margin-left:10px; width: 80px;" value="Delete" ng-click="grid.appScope.DeleteBook(row.entity)"></div>' },
                        { name: 'Full Name', field: 'Full Name' , cellTemplate: '<span>{{row.entity.Name}} {{row.entity.Surname}}</span>', cellClass: "ui-grid-cell-contents" },
                        { name: 'Title', field: 'Title' },
                        { name: 'Author', field: 'Author' },
                        { name: 'Year', field: 'Year' },
                        { name: 'Amount', field: 'Amount' },
                        { name: '', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Select" ng-click="grid.appScope.SelectLends(row.entity)"></div>' },
                        { name: ' ', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Delete" ng-click="grid.appScope.DeleteLends(row.entity)"></div>' }
                    ]
            }
    }

    GetAllMembers()
    {
        this.MembersSvc.GetAllAsync()
            .then((response) =>
            {
                this.OnGetMembers(response);
            });
    }

    OnGetMembers(response)
    {
        this.Members.length = 0;

        for (let i in response.data)
        {
            let member = new Member(response.data[i]);
            this.Members.push(member);
        }
    }

    GetAllBooks()
    {
        this.BooksSvc.GetAllAsync()
            .then((response) =>
            {
                this.OnGetBooks(response);
            });
    }

    OnGetBooks(response)
    {
        this.Books.length = 0;
        for (let i in response.data)
        {
            let bookAsJson = response.data[i];
            let book = new Book(bookAsJson);
            this.Books.push(book);
        }
    }

    GetMemberLends()
    {
        this.LendsSvc.GetLendsByMember(this.SelectedMember.Id)
            .then((response) =>
            {
                this.OnGetMemberLends(response);
            });
    }

    OnGetMemberLends(response)
    {
        this.Lends.length = 0;

        for (let i in response.data)
        {
            let lendAsJson = response.data[i];
            let lend = new Lend(lendAsJson);
            this.Lends.push(lend);
        }
    }
    
    RequestLend()
    {
        let lend = new Lend();
        lend.BookId = this.SelectedBook.Id;
        lend.MemberId = this.SelectedMember.Id;
        lend.DaysLended = 30;

        this.LendsSvc.AddAsync(lend).
            then((response) =>
            {
                this.GetMemberLends();
            });
    }
}

app.component('lends',
    {
        templateUrl: './Scripts/views/Home/Lends/LendsView.html',
        controller: ['$MembersService', '$BooksService', '$LendsService', LendsViewModel],
        controllerAs: "vm"
    });



﻿class UsersViewModel
{
    constructor($UsersService)
    {
        this.UsersSvc = $UsersService;
        this.GetAllUsers();
        this.Users = [];

        this.SelectedUser = null;

        this.GridOptions =
            {
                enableFiltering: false,
                enableSorting: false,
                data: 'vm.Users',
                appScopeProvider: this,
                columnDefs:
                    [
                        { name: 'Full Name', field: 'Full Name', cellTemplate: '<span>{{row.entity.Name}} {{row.entity.Surname}}</span>', cellClass: "ui-grid-cell-contents" },
                        { name: 'Email', field: 'Email' },
                        { name: 'UserType', field: 'UserType'}
                    ]
            }
    }

    GetAllUsers()
    {
        this.UsersSvc.GetAllAsync()
            .then((response) =>
            {
                this.OnGetData(response);
            },
            response => console.log(response));
    }

    OnGetData(response)
    {
        this.Users.length = 0; 
        for (let i in response.data)
        {
            let user = new User(response.data[i]);
            this.Users.push(user);
        }
    }

    AddNewUser()
    {
        let user = new User();

        user.Name = this.Name;
        user.Surname = this.Surname;
        user.Email = this.Email;
        user.Password = this.Password;

        this.UsersSvc.AddAsync(user)
            .then((response) =>
            { this.OnUserAdded(response); },
                response => console.log("error"));
    }

    OnUserAdded(response)
    {
        let user = new User(response.data);
        this.Users.push(user);
        console.log(response.data);
        this.ClearForm();
    }

    SelectUser(user)
    {
        this.SelectedUser = user;

        this.Name = user.Name;
        this.Surname = user.Surname;
        this.Email = user.Email;
        //this.Password = user.Password;
    }

    UpdateUser()
    {
        this.SelectedUser.Name = this.Name;
        this.SelectedUser.Surname = this.Surname;
        this.SelectedUser.Email = this.Email;
        this.SelectedUser.Password = this.Password;

        this.UsersSvc.UpdateAsync(this.SelectedUser)
            .then((response) =>
            {
                //response.data = this.SelectedUser;
                this.OnUserUpdated(response);
            },
                response => console.log(response));
    }

    OnUserUpdated(response)
    {
        //let user = new User(response.data);
        //let index = this.Users.findIndex(x => x.Id == this.SelectedUser.Id);
        //this.Users[index] = user;

        //console.log(response.data);

        this.ClearForm();
    }

    DeleteUser(user)
    {
        this.UsersSvc.DeleteAsync(user)
            .then((response) =>
            {
                this.OnUserDeleted(user);
                console.log(user);
            },
                (response) => console.log(response));
    }

    OnUserDeleted(user)
    {
        let i = this.Users.findIndex((x) => x.Id === user.Id);
        this.Users.splice(i, 1);
    }

    ClearForm()
    {
        this.Name = "";
        this.Surname = "";
        this.Email = "";
        this.Password = "";

        this.IsEditing = false;

        Name.classList.add("ng-pristine");
        Surname.classList.add("ng-pristine");
        Email.classList.add("ng-pristine");
        Password.classList.add("ng-pristine");
    }
}

app.component('users',
    {
        templateUrl: './Scripts/views/Home/Users/UsersView.html',
        controller: UsersViewModel,
        controllerAs: "vm"
    });



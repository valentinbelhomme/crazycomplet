﻿class MembersViewModel
{
    constructor($MembersService)
    {
        this.MembersSvc = $MembersService;

        this.GetAllMembers();
        this.Members = [];

        this.SelectedMember = null;
        this.IsEditing = false;

        this.GridOptions =
            {
                enableFiltering: false,
                enableSorting: false,
                data: 'vm.Members',
                appScopeProvider: this,
                columnDefs:
                    [
                        { name: 'Full Name', field: 'Full Name', cellTemplate: '<span>{{row.entity.Name}} {{row.entity.Surname}}</span>', cellClass: "ui-grid-cell-contents"},
                        { name: 'Email', field: 'Email' },
                        { name: '', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Select" ng-click="grid.appScope.SelectMembers(row.entity)"></div>' },
                        { name: ' ', field: 'Id', cellTemplate: '<div class="ui-grid-cell-contents"><input type="button" value="Delete" ng-click="grid.appScope.DeleteMembers(row.entity)"></div>' }
                    ]
            }
    }

    GetAllMembers()
    {
        this.MembersSvc.GetAllAsync()
            .then((response) =>
            {
                this.OnGetData(response);
            });
    }

    OnGetData(response)
    {
        this.Members.length = 0;
        for (let i in response.data)
        {
            let member = new Member(response.data[i]);
            this.Members.push(member);
        }
    }

    AddNewMember()
    {
        let member = new Member();

        member.Name = this.Name;
        member.Surname = this.Surname;
        member.Email = this.Email;
        member.Password = this.Password;

        this.AddMember(member);
    }

    AddMember(member)
    {       
        this.MembersSvc.AddAsync(member)
        //var config = { headers: { 'Content-Type': 'application/json' } };
        //this.Http.post("api/clients", client, config)
            .then((response) =>
            { this.OnMemberAdded(response); },
                response => console.log(response));
    }

    OnMemberAdded(response)
    {
        let member = new Member(response.data);
        this.Members.push(member);
        this.ClearForm();
    }

    SelectMembers(member)
    {
        this.SelectedMember = member;

        this.Name = member.Name;
        this.Surname = member.Surname;
        this.Email = member.Email;
        this.Password = member.Password;

        this.IsEditing = true;
    }

    UpdateMember()
    {
        this.SelectedMember.Name = this.Name;
        this.SelectedMember.Surname = this.Surname;
        this.SelectedMember.Email = this.Email;
        this.SelectedMember.Password = this.Password;

        this.MembersSvc.UpdateAsync(this.SelectedMember)
            .then((response) =>
            {   //response.data = this.SelectedMember;
                this.OnMemberUpdated(response);
            },
                response => console.log(response));
    }

    OnMemberUpdated(response)
    {
        //let member = new Member(response.data);
        //let index = this.Members.findIndex(x => x.Id == this.SelectedMember.Id);
        //this.Members[index] = member;

        //console.log(response.data);

        this.ClearForm();
    }

    DeleteMembers(member)
    {
        //let url = "api/clients/" + client.Id;
        //this.Http.delete(url)
        this.MembersSvc.DeleteAsync(member)
            .then((response) =>
            {
                this.OnMemberDeleted(member);
                console.log(member);
            },
                (response) => console.log(response));
    }

    OnMemberDeleted(member)
    {
        let i = this.Members.findIndex((x) => x.Id === member.Id);
        this.Members.splice(i, 1);
    }

    ClearForm()
    {
        this.Name = "";
        this.Surname = "";
        this.EmailClass = "";
        this.PasswordClass = "";

        this.IsEditing = false;

        Name.classList.add("ng-pristine");
        Surname.classList.add("ng-pristine");
        this.EmailClass = "ng-pristine";
        this.PasswordClass = "ng-pristine";
    }
}

app.component('members',
    {
        templateUrl: './Scripts/views/Home/Members/MembersView.html',
        controller: MembersViewModel,
        controllerAs: "vm"
    });



﻿class LendsService extends CRUDService
{
    constructor($http, $window)
    {
        super($http, $window, "lends");
        //this.Http = $http;
    }

    GetLendsByMember(memberId) {
        return this.Http.get(this.ApiUrl + "/ByMember/" + memberId, this.Config);
    }
}

//Dire à angular qu'on cree un service $AdminsService
// pour qu'il l'injecte dans les controllers
app.service('$LendsService', LendsService);
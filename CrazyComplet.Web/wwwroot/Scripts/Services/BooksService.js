﻿class BooksService extends CRUDService
{
    constructor($http, $window)
    {
        super($http, $window, "books");
        //this.Http = $http;
    }
}

//Dire à angular qu'on cree un service $AdminsService
// pour qu'il l'injecte dans les controllers
app.service('$BooksService', BooksService);
﻿class GenericService
{
    get Config()
    {
        var config =
        {
            headers:
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.Token
            }
        };
        return config;
    }

    constructor($http, $window, dtoName)
    {
        this.Http = $http;
        this.Token = $window.Token;
        this.ApiUrl = "api/" + dtoName;
    }
    GetGenericAllAsync()
    {
        return this.Http.get(this.ApiUrl, this.Config);
    }

    GetByIdAsync(id)
    {
        return this.Http.get(this.ApiUrl + "/" + id, this.Config);
    }

    PostAsync(dto)
    {

        return this.Http.post(this.ApiUrl, dto, this.Config)
    }

    PutAsync(dto)
    {
        //let urlId = this.ApiUrl + dto.Id;
        //return this.Http.put(urlId, dto, this.Config)
        return this.Http.put(this.ApiUrl, dto, this.Config)
    }

    DeleteGenericAsync(model)
    {
        let urlId = this.ApiUrl + "/" +  model.Id;
        return this.Http.delete(urlId)
    }
}
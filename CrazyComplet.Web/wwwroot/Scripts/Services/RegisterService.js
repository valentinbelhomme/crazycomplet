﻿class RegisterService extends GenericService
{
    constructor($http, $window)
    {
        super($http, $window, 'register');
    }

    RegisterAsync(email, password, name, surname)
    {
        var registerRqst = new RegisterRequest(email, password, name, surname);
        return this.PostAsync(registerRqst)
    }
    //{
    //    var request = new RegisterRequest(email, password);
    //    return this.PostAsync(request);
    //}
}

// esto le dice a Angular que creamos un service que se llama $UsersService
// para que lo inyecte 
app.service('$RegisterService', RegisterService);
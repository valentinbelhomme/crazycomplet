﻿class CRUDService extends GenericService
{
    constructor($http, $window, model)
    {
        super($http, $window, model);
    }

    GetAllAsync()
    {
        return this.GetGenericAllAsync(this.ApiUrl);
    }

    AddAsync(model)
    {
        return this.PostAsync(model)
    }
    
    UpdateAsync(SelectedModel)
    {
        return this.PutAsync(SelectedModel);
    }

    DeleteAsync(model)
    {
        return this.DeleteGenericAsync(model)
    }
}


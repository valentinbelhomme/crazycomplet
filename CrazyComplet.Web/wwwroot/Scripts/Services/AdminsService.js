﻿class AdminsService extends CRUDService
{
    constructor($http, $window)
    {
        super($http, $window, "admins");
    }
}

//Dire à angular qu'on cree un service $AdminsService
// pour qu'il l'injecte dans les controllers
app.service('$AdminsService', AdminsService);
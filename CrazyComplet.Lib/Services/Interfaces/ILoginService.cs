﻿using CrazyComplet.Lib.Models;
using CrazyComplet.Lib.Services.Dtos;

namespace CrazyComplet.Lib.Services
{
    public interface ILoginService
    {
        User Authenticate(LoginRequest loginRequest);
    }
}

﻿using CrazyComplet.Lib.Models;
using CrazyComplet.Lib.Services.Dtos;

namespace CrazyComplet.Lib.Services
{
    public interface IRegisterService
    {
        RegisterResponse Register(RegisterRequest registerRequest);
    }
}

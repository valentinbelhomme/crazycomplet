﻿using System;
using CrazyComplet.Lib.Models;
using System.Collections.Generic;
using System.Text;

namespace CrazyComplet.Lib.Services
{
    public class RegisterResponse
    {
        public RegisterResponseStatus Status { get; set; }
        public Member Member { get; set; }
    }
    public enum RegisterResponseStatus
    {
        Ok,
        UserWithEmailAlreadyExists,
        WrongEmail,
        MissingEmail,
        MissingPassword,
        PasswordInsecure
    }
}

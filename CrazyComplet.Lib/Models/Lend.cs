﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrazyComplet.Lib.Models
{
    public class Lend : Entity
    {
        public Guid UserId { get; set; }
        public Guid BookId { get; set; }

        public DateTime? LendedOn { get; set; }
        public DateTime? ExpiresOn { get; set; }

        [JsonIgnore]
        public User User { get; set; }

        [JsonIgnore]
        public Book Book { get; set; }

        public string BookComposedName
        {
            get
            {
                return Book.Title + ", " + Book.Author;
            }
        }
    }
}

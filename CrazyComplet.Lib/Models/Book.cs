﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrazyComplet.Lib.Models
{
    public class Book : Entity
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }
        public int Amount { get; set;}

        [JsonIgnore]
        public ICollection<Lend> Lends { get; set; }
    }
}

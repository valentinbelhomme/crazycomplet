﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrazyComplet.Lib.Models
{
    public class User : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }

        [JsonIgnore]
        public string Password { get; set; }

        public string UserType
        { get
            {
                return this.GetType().Name;
            }
        }

        public string Token { get; set; }

        [JsonIgnore]
        public ICollection<Request> Requests { get; set; }
        [JsonIgnore]
        public ICollection<Lend> Lends { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrazyComplet.Lib.Models
{
    public class Room : Entity
    {
        public string Code { get; set; }
        public string Color { get; set; }
        public int Capacity { get; set;}
        public bool Accessibility { get; set; }

        [JsonIgnore]
        public ICollection<Request> Requests { get; set; }
    }
}

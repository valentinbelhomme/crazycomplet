﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrazyComplet.Lib.Models
{
    public class Request : Entity
    {
        public Guid UserId { get; set; }
        public Guid RoomId { get; set; }

        public DateTime? HourIn { get; set; }
        public DateTime? HourOff { get; set; }  

        [JsonIgnore]
        public User User { get; set; }

        [JsonIgnore]
        public Room Room { get; set; }
    }
}

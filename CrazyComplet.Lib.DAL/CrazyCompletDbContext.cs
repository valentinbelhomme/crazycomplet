﻿using System;
using CrazyComplet.Lib.Models;
using Microsoft.EntityFrameworkCore;


namespace CrazyComplet.Lib.DAL
{
    public class CrazyCompletDbContext : DbContext
    {
        public CrazyCompletDbContext(DbContextOptions<CrazyCompletDbContext> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Lend> Lends { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Room> Rooms { get; set; }


        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Lend>()
        //        .HasKey(bu => new { bu.BookId, bu.UserId });
        //    modelBuilder.Entity<Lend>()
        //        .HasOne(bu => bu.Book)
        //        .WithMany(b => b.Lends)
        //        .HasForeignKey(bu => bu.BookId);
        //    modelBuilder.Entity<Lend>()
        //            .HasOne(bu => bu.User)
        //            .WithMany(u => u.Lends)
        //            .HasForeignKey(bu => bu.UserId);

        //    modelBuilder.Entity<Request>()
        //        .HasKey(ru => new { ru.RoomId, ru.UserId });
        //    modelBuilder.Entity<Request>()
        //        .HasOne(ru => ru.Room)
        //        .WithMany(r => r.Requests)
        //        .HasForeignKey(ru => ru.RoomId);
        //    modelBuilder.Entity<Request>()
        //            .HasOne(ru => ru.User)
        //            .WithMany(u => u.Requests)
        //            .HasForeignKey(ru => ru.UserId);

        //}


    }
}

